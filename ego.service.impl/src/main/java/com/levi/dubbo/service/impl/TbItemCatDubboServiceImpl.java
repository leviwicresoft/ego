package com.levi.dubbo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.levi.dubbo.service.TbItemCatDubboService;
import com.levi.mapper.TbItemCatMapper;
import com.levi.pojo.TbItemCat;
import com.levi.pojo.TbItemCatExample;

public class TbItemCatDubboServiceImpl implements TbItemCatDubboService{
	
	@Resource
	private TbItemCatMapper tbItemCatMapper;

	public List<TbItemCat> show(long pid) {
		TbItemCatExample example = new TbItemCatExample();
		example.createCriteria().andParentIdEqualTo(pid);
		return tbItemCatMapper.selectByExample(example);
	}

	public TbItemCat selById(long id) {
		return tbItemCatMapper.selectByPrimaryKey(id);
	}
	

}
