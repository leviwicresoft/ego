package com.levi.dubbo.service.impl;


import java.util.List;

import javax.annotation.Resource;

import com.levi.dubbo.service.TbItemParamItemDubboService;
import com.levi.mapper.TbItemParamItemMapper;
import com.levi.pojo.TbItemParamItem;
import com.levi.pojo.TbItemParamItemExample;

public class TbItemParamItemDubboServiceImpl implements TbItemParamItemDubboService{

	@Resource
	TbItemParamItemMapper tbItemParamItemMapper;
	
	public TbItemParamItem selByItemId(long id) {
		TbItemParamItemExample example = new TbItemParamItemExample();
		example.createCriteria().andItemIdEqualTo(id);
		List<TbItemParamItem> list = tbItemParamItemMapper.selectByExampleWithBLOBs(example);
		
		if(list!=null&&list.size()>0){
			return list.get(0);
		}
		
		return null;
	}


}
