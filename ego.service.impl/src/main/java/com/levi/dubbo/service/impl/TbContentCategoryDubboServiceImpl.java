package com.levi.dubbo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.levi.dubbo.service.TbContentCategoryDubboService;
import com.levi.mapper.TbContentCategoryMapper;
import com.levi.pojo.TbContentCategory;
import com.levi.pojo.TbContentCategoryExample;

public class TbContentCategoryDubboServiceImpl implements TbContentCategoryDubboService{

	@Resource
	private TbContentCategoryMapper tbContentCategoryMapper;
	
	public List<TbContentCategory> selByPid(long id) {
		// TODO Auto-generated method stub
		TbContentCategoryExample example = new TbContentCategoryExample();
		example.createCriteria().andParentIdEqualTo(id);
		return tbContentCategoryMapper.selectByExample(example);
	}

	public int insTbContentCategory(TbContentCategory cate) {
		return tbContentCategoryMapper.insertSelective(cate);
	}

	public int updIsParentById(TbContentCategory cate) {
		return tbContentCategoryMapper.updateByPrimaryKeySelective(cate);
	}

	public TbContentCategory selById(long id) {
		return tbContentCategoryMapper.selectByPrimaryKey(id);
	}

}
