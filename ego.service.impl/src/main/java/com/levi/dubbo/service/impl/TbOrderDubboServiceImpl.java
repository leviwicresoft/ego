package com.levi.dubbo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.levi.dubbo.service.TbOrderDubboService;
import com.levi.mapper.TbOrderItemMapper;
import com.levi.mapper.TbOrderMapper;
import com.levi.mapper.TbOrderShippingMapper;
import com.levi.pojo.TbOrder;
import com.levi.pojo.TbOrderItem;
import com.levi.pojo.TbOrderShipping;

public class TbOrderDubboServiceImpl implements TbOrderDubboService{

	@Resource
	private TbOrderMapper tbOrderMapper;
	@Resource 
	private TbOrderItemMapper tbOrderItemMapper;
	@Resource
	private TbOrderShippingMapper tbOrderShippingMapper;
	
	public int insOrder(TbOrder order, List<TbOrderItem> list, TbOrderShipping shipping) throws Exception {
		int index = 0;
		 index = tbOrderMapper.insert(order);
		 for (TbOrderItem orderItem : list){
			 index += tbOrderItemMapper.insert(orderItem);
		 }
		 index+= tbOrderShippingMapper.insert(shipping);
		 if(index == 2+ list.size()){
			 return 1;
		 }else{
			 throw new Exception("订单保存失败！！");
		 }
		}

}
