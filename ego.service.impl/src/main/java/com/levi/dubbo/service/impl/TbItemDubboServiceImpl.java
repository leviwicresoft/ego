package com.levi.dubbo.service.impl;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.dubbo.service.TbItemDubboService;
import com.levi.mapper.TbItemDescMapper;
import com.levi.mapper.TbItemMapper;
import com.levi.mapper.TbItemParamItemMapper;
import com.levi.pojo.TbItem;
import com.levi.pojo.TbItemDesc;
import com.levi.pojo.TbItemExample;
import com.levi.pojo.TbItemParamItem;
@Service
public class TbItemDubboServiceImpl implements TbItemDubboService{
	
	@Resource
	private TbItemMapper tbItemMapper;
	
	@Resource
	private TbItemDescMapper tbItemDescMapper;
	
	@Resource
	private TbItemParamItemMapper tbItemParamItemMapper;
	
	public EasyUIDataGrid show(int page, int rows) {
		PageHelper.startPage(page, rows);
		
		List<TbItem> list = tbItemMapper.selectByExample(new TbItemExample());
		PageInfo<TbItem> pi = new PageInfo(list);
		EasyUIDataGrid dataGrid = new EasyUIDataGrid();
		dataGrid.setRows(pi.getList());
		dataGrid.setTotal(pi.getTotal());
		return dataGrid;
	}

	public int updItemStatus(TbItem tbItem) {
		// TODO Auto-generated method stub
		return tbItemMapper.updateByPrimaryKeySelective(tbItem);
		
	}
    //增加
	public int insTbItem(TbItem tbItem){
		return tbItemMapper.insert(tbItem);
	}
	//实现事务
	//新增商品描述和参数
	
	public int insTbItemDesc(TbItem tbItem, TbItemDesc tbItemDesc, TbItemParamItem paramItems) throws Exception{
		int index = 0;
		try{
			index = tbItemMapper.insertSelective(tbItem);
			index += tbItemDescMapper.insertSelective(tbItemDesc);
			index += tbItemParamItemMapper.insertSelective(paramItems);
		}catch(Exception e){
			
		}
		if(index ==3){
			return 1;
		}else{
			
			throw new Exception("Upload failed");
		}
	}

	public List<TbItem> selAllByStatus(byte status) {
		TbItemExample example = new TbItemExample();
		example.createCriteria().andStatusEqualTo(status);
		return tbItemMapper.selectByExample(example);
	}

	public TbItem selById(long id) {
		return tbItemMapper.selectByPrimaryKey(id);
	}
}
