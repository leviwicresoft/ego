package com.levi.dubbo.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.dubbo.service.TbItemParamDubboService;
import com.levi.mapper.TbItemParamMapper;
import com.levi.pojo.TbItemParam;
import com.levi.pojo.TbItemParamExample;

public class TbItemParamDubboServiceImpl implements TbItemParamDubboService {

	@Resource
	private TbItemParamMapper tbItemParamMapper;
	
	public EasyUIDataGrid showPage(int page, int rows) {
		PageHelper.startPage(page, rows);
		
		//withBLOBS() 查询带有text类型的行
		List<TbItemParam> list = tbItemParamMapper.selectByExampleWithBLOBs(new TbItemParamExample());
		PageInfo<TbItemParam> pi = new PageInfo(list);
		EasyUIDataGrid dataGrid = new EasyUIDataGrid();
		dataGrid.setRows(pi.getList());
		dataGrid.setTotal(pi.getTotal());
		return dataGrid;
	}
	
	public int delByIds(String ids) throws Exception{
		String [] idStr = ids.split(",");
		int index = 0;
		for(String id:idStr){
			index += tbItemParamMapper.deleteByPrimaryKey(Long.parseLong(id));
		}
		if(index == idStr.length){
			return 1;
		}else{
			throw new Exception("Failed to batch delete Params");
		}
		}
	
	public TbItemParam selByCatId(long catId) {
		// TODO Auto-generated method stub
		TbItemParamExample example = new TbItemParamExample();
		example.createCriteria().andIdEqualTo(catId);
		List<TbItemParam> list =  tbItemParamMapper.selectByExampleWithBLOBs(example);
		if(list != null && list.size() >0){
			return list.get(0);
		}
		return null;
	}
	
	public int insParam(TbItemParam param) {
		return tbItemParamMapper.insertSelective(param);
	}
	
	
}
