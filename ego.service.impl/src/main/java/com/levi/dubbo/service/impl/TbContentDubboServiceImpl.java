package com.levi.dubbo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.dubbo.service.TbContentDubboService;
import com.levi.mapper.TbContentMapper;
import com.levi.pojo.TbContent;
import com.levi.pojo.TbContentExample;

public class TbContentDubboServiceImpl implements TbContentDubboService{
	
	@Resource
	private TbContentMapper tbContentMapper;

	public EasyUIDataGrid selContentById(long categoryId, int page, int rows) {
		PageHelper.startPage(page,rows);
		
		TbContentExample exmaple = new TbContentExample();
		if(categoryId!=0){
			exmaple.createCriteria().andCategoryIdEqualTo(categoryId);
		}
		
		List<TbContent> list = tbContentMapper.selectByExampleWithBLOBs(exmaple);
		
		PageInfo<TbContent> pi = new PageInfo(list);
		EasyUIDataGrid datagrid = new EasyUIDataGrid();
		datagrid.setRows(pi.getList());
		datagrid.setTotal(pi.getTotal());
		return datagrid;
	}

	public int insContent(TbContent content) {
		return tbContentMapper.insertSelective(content);
	}

	public List<TbContent> selByCount(int count, boolean isSort) {
		TbContentExample example = new TbContentExample();
		if(isSort){
			example.setOrderByClause("updated desc");
		}
		if(count!=0){
			PageHelper.startPage(1, count);
			List<TbContent> list = tbContentMapper.selectByExampleWithBLOBs(example);
			PageInfo<TbContent> pi = new PageInfo(list);
			return pi.getList();
		}else{
		return tbContentMapper.selectByExampleWithBLOBs(example);
		}
	}
}
