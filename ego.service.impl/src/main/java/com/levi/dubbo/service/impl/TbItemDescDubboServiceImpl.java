package com.levi.dubbo.service.impl;

import javax.annotation.Resource;

import com.levi.dubbo.service.TbItemDescDubboService;
import com.levi.mapper.TbItemDescMapper;
import com.levi.pojo.TbItemDesc;

public class TbItemDescDubboServiceImpl implements TbItemDescDubboService{
	@Resource
	private TbItemDescMapper tbItemDescMapper;
	

	public int insTbItemDesc(TbItemDesc tbItemDesc) {
		// TODO Auto-generated method stub
		return tbItemDescMapper.insert(tbItemDesc);

	}

	public TbItemDesc selByItemId(long id) {
		// TODO Auto-generated method stub
		return tbItemDescMapper.selectByPrimaryKey(id);
	}

}
