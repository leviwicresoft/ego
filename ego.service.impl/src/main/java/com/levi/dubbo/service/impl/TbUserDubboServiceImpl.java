package com.levi.dubbo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.levi.dubbo.service.TbUserDubboService;
import com.levi.mapper.TbUserMapper;
import com.levi.pojo.TbUser;
import com.levi.pojo.TbUserExample;

public class TbUserDubboServiceImpl implements TbUserDubboService {
	@Resource
	private TbUserMapper tbUserMapper;

	public TbUser selByTbUser(TbUser tbUser) {
		TbUserExample example = new TbUserExample();
		example.createCriteria().andUsernameEqualTo(tbUser.getUsername()).andPasswordEqualTo(tbUser.getPassword());
		List<TbUser> list = tbUserMapper.selectByExample(example);
		if(list!=null &&list.size()>0){
			return list.get(0);
		}
		return null;
	}

}
