package com.levi.commons.pojo;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.alibaba.fastjson.JSON;

public class HttpClientUtil {
	
	private static CloseableHttpClient client = HttpClients.createDefault();
	
	public static void doPostJson(String url, Map<String, Object> map) throws Exception{
		HttpPost post = new HttpPost(url);
		post.setHeader("Content-Type", "application/json;charset=utf-8");
		post.setEntity(new StringEntity(JSON.toJSONString(map)));
		CloseableHttpResponse response = client.execute(post);
		int status = response.getStatusLine().getStatusCode();
		post.releaseConnection();
		
	}
	
	public static String doGet(String url) throws Exception{
		HttpGet get = new HttpGet(url);
		CloseableHttpResponse response = client.execute(get);
		HttpEntity entity = response.getEntity();
		return entity.toString();
	}
}
