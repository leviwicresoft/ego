package com.levi.commons.pojo;

import com.levi.pojo.TbItem;

public class TbItemChild extends TbItem{
	
	private String[] images;
	private Boolean inStock;
	

	public Boolean getInStock() {
		return inStock;
	}

	public void setInStock(Boolean inStock) {
		this.inStock = inStock;
	}

	public String[] getImages() {
		return images;
	}

	public void setImages(String[] images) {
		this.images = images;
	}
	
}
