package com.levi.commons.pojo;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtil {
	public static String getCookie(HttpServletRequest req, HttpServletResponse res, String key){
		Cookie[] cs = req.getCookies();
		for(Cookie c :cs){
			if(c.getName().equals(key)){
				return c.getValue();
			}
		}
		return null;
	}
	public static void setCookie(HttpServletRequest req, HttpServletResponse res, String key, String value, int maxAge){
		Cookie c = new Cookie(key, value);
		if(maxAge>0){
			c.setMaxAge(maxAge);
		}
			c.setDomain(req.getServerName());
			res.addCookie(c);
		
	}
	
	public static void delCookie(HttpServletRequest req, HttpServletResponse res, String key, String value){
		Cookie c = new Cookie(key, value);
		c.setMaxAge(0);
		c.setDomain(req.getServerName());
		res.addCookie(c);
	}
}
