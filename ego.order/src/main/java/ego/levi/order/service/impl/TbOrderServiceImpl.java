package ego.levi.order.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jackrabbit.uuid.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.levi.commons.pojo.CookieUtil;
import com.levi.commons.pojo.EgoResult;
import com.levi.commons.pojo.HttpClientUtil;
import com.levi.commons.pojo.TbItemChild;
import com.levi.dubbo.service.TbItemDubboService;
import com.levi.dubbo.service.TbOrderDubboService;
import com.levi.order.pojo.OrderParam;
import com.levi.order.service.TbOrderService;
import com.levi.pojo.TbItem;
import com.levi.pojo.TbOrder;
import com.levi.pojo.TbOrderItem;
import com.levi.pojo.TbOrderShipping;
import com.levi.pojo.TbUser;
import com.levi.redis.dao.RedisDao;
@Service
public class TbOrderServiceImpl implements TbOrderService{
	
	@Reference
	private TbItemDubboService tbItemDubboServiceImpl;
	@Reference
	private TbOrderDubboService tbOrderDubboServiceImpl;
	
	@Value("${passport.url}")
	private String passportUrl;
	
	@Value("${cart.user}")
	private String userKey;
	
	@Resource
	private RedisDao redisDaoImpl;

	public List<TbItemChild> showOrderCart(List<Long> ids, HttpServletRequest req, HttpServletResponse res) {
		String token = CookieUtil.getCookie(req, res, "TT_TOKEN");
		EgoResult er = new EgoResult();
		if(token != null && !token.equals("")){
			try {
				String json = HttpClientUtil.doGet(passportUrl+token);
				if(json!=null&&!json.equals("")){
					er= JSON.parseObject(json, EgoResult.class);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}}
			//获取redis 购物车key
			 String key = userKey+((TbUser)er.getData()).getUsername();
			 String order = redisDaoImpl.get(key);
			 List<TbItemChild> list= new ArrayList();
			 if(order != null &&! order.equals("")){
				 List<TbItemChild> children = JSON.parseArray(order, TbItemChild.class);
				 for(TbItemChild child : children){
					 for(long id : ids){
						 if(child.getId() == id){
							 TbItem item = tbItemDubboServiceImpl.selById(id);
							 if(child.getNum()>item.getNum()){
								 child.setInStock(false);
							 }else{
								 child.setInStock(true);
							 }
							 list.add(child);
						 }
					 }
				 }
			 }
			 return list;
		}

	public EgoResult createOrder(OrderParam param, HttpServletRequest req, HttpServletResponse res) {
		TbOrder order = new TbOrder();
		order.setPayment(param.getPayment());
		order.setPaymentType(param.getPaymentType());
		long id = Long.parseLong(UUID.randomUUID().toString());
		order.setOrderId(id+"");
		Date date = new Date();
		order.setCreateTime(date);
		order.setUpdateTime(date);
		TbUser user= null;
		String token = CookieUtil.getCookie(req, res, "TT_TOKEN");
		try {
			String er = HttpClientUtil.doGet(passportUrl + token);
			EgoResult result = JSON.parseObject(er, EgoResult.class);
			user = (TbUser)result.getData();
		} catch (Exception e) {
			e.printStackTrace();
		}
		order.setUserId(user.getId());
		order.setBuyerNick(user.getUsername());
		order.setBuyerRate(0);
		
		for(TbOrderItem orderItem : param.getOrderItems()){
			orderItem.setId(UUID.randomUUID().toString());
			orderItem.setOrderId(id +"");
		}
		
		TbOrderShipping shipping = param.getOrderShipping();
		shipping.setOrderId(id +"");
		shipping.setCreated(date);
		shipping.setUpdated(date);
		int index = 0;
		try {
			index = tbOrderDubboServiceImpl.insOrder(order, param.getOrderItems(), shipping);
		} catch (Exception e) {
			e.printStackTrace();
		}
		EgoResult er = new EgoResult();
		if (index ==1){
			er.setStatus(200);
		}
		return er;
	}
	}


