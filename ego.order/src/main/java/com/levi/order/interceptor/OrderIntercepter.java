package com.levi.order.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.levi.commons.pojo.CookieUtil;
import com.levi.commons.pojo.EgoResult;
import com.levi.commons.pojo.HttpClientUtil;

public class OrderIntercepter implements HandlerInterceptor {

	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler)
			throws Exception {
		String token = CookieUtil.getCookie(req, res, "TT_TOKEN");
		if(token!=null&&!token.equals("")){
		String result = HttpClientUtil.doGet("http://localhost:8084/user/token/" + token);
		EgoResult er = JSON.parseObject(result, EgoResult.class);
		if(er.getStatus()==200){
			return true;
		}
		}
		String num = req.getParameter("num");
		if(num!=null&&!num.equals("")){
		res.sendRedirect("http://localhost:8084/user/showLogin?interUrl=" + req.getRequestURL()+"%3Fnum=" +num);
		}else{
			res.sendRedirect("http://localhost:8084/user/showLogin");
		}
		return false;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
	}

}
