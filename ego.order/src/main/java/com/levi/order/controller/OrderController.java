package com.levi.order.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.levi.commons.pojo.TbItemChild;
import com.levi.order.pojo.OrderParam;
import com.levi.order.service.TbOrderService;

@Controller
public class OrderController {
	@Resource
	private TbOrderService tbOrderServiceImpl;
	
	//订单确认页面
	@RequestMapping("order/oder-cart.html")
	public String showCartOrder(Model model,@RequestParam("id")List<Long> ids, HttpServletRequest req, HttpServletResponse res){
		List<TbItemChild> list = tbOrderServiceImpl.showOrderCart(ids, req, res);
		model.addAttribute("cartList", list);
		return "order-cart";
	}
	
	@RequestMapping("order/create.html")
	public String createOrder(OrderParam param, HttpServletRequest req, HttpServletResponse res){
		tbOrderServiceImpl.createOrder(param, req, res);
		return "my-orders";
	}

}
