package com.levi.order.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.levi.commons.pojo.EgoResult;
import com.levi.commons.pojo.TbItemChild;
import com.levi.order.pojo.OrderParam;

public interface TbOrderService {

	List<TbItemChild> showOrderCart(List<Long> ids, HttpServletRequest req, HttpServletResponse res);
	
	EgoResult createOrder(OrderParam param, HttpServletRequest req, HttpServletResponse res);
	
}
