package com.levi.user.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.levi.commons.pojo.EgoResult;
import com.levi.pojo.TbUser;

public interface TbUserService {
	EgoResult login(TbUser user,HttpServletRequest req, HttpServletResponse res);
	
	//根据cookie token，获取用户信息
	EgoResult getUserInfoByToken(String token);
	
	EgoResult logout(String token,HttpServletRequest req, HttpServletResponse res);
}
