package com.levi.user.service.impl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jackrabbit.uuid.UUID;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.levi.commons.pojo.CookieUtil;
import com.levi.commons.pojo.EgoResult;
import com.levi.dubbo.service.TbUserDubboService;
import com.levi.pojo.TbUser;
import com.levi.redis.dao.RedisDao;
import com.levi.user.service.TbUserService;

@Service
public class TbUserServiceImpl implements TbUserService{

	@Reference 
	private TbUserDubboService  tbUserDubboServiceImpl;
	@Resource 
	private RedisDao redisDaoImpl;
	
	public EgoResult login(TbUser tbUser, HttpServletRequest req, HttpServletResponse res) {
		EgoResult er = new EgoResult();
		TbUser user = tbUserDubboServiceImpl.selByTbUser(tbUser);
		if(user != null){
			er.setStatus(200);
			String uuid = UUID.randomUUID().toString();
			int maxAge = 60*60*24;
			redisDaoImpl.expire(uuid, maxAge);
			
			//设置缓存SSO
			redisDaoImpl.set(uuid, JSON.toJSONString(user));
			CookieUtil.setCookie(req, res,"TT_TOKEN", uuid, maxAge);
			
		}else{
			er.setMsg("登录失败");
		}
		return er;
	}

	public EgoResult getUserInfoByToken(String token) {
		EgoResult er = new EgoResult();
		if(redisDaoImpl.exists(token)){
			TbUser user = JSON.parseObject(redisDaoImpl.get(token), TbUser.class);
			user.setPassword(null);
			er.setMsg("OK");
			er.setStatus(200);
			er.setData(user);
		}else{
			er.setMsg("获取用户失败");
		}
		return er;
	}

	public EgoResult logout(String token,HttpServletRequest req, HttpServletResponse res) {
		redisDaoImpl.del(token);
		CookieUtil.delCookie(req, res, "TT_TOKEN", "");
		EgoResult er= new EgoResult();
		er.setStatus(200);
		er.setMsg("OK");
		return er;
	}

}
