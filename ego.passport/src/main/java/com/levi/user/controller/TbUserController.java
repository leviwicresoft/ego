package com.levi.user.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.levi.commons.pojo.EgoResult;
import com.levi.pojo.TbUser;
import com.levi.user.service.TbUserService;

@Controller
public class TbUserController {

	@Resource
	private TbUserService tbUserServiceImpl;
	
	@RequestMapping("user/showLogin")
	public String showLogin(@RequestHeader(value = "Referer", defaultValue="") String url, Model model, String interUrl){
		if(interUrl!=null&&!interUrl.equals("")){
			model.addAttribute("redirect", interUrl);
		}else if(url!=null && !url.equals("")){
			
			model.addAttribute("redirect", url);
		}
		return "login";
	}
	@RequestMapping("user/login")
	@ResponseBody
	public EgoResult login(TbUser tbUser, HttpServletRequest req, HttpServletResponse res){
		return tbUserServiceImpl.login(tbUser, req, res);
	}
	
	//根据token 获取用户信息
	@RequestMapping("user/token/{token}")
	@ResponseBody
	public Object getUserInfo(@PathVariable String token, String callback){
		EgoResult er = tbUserServiceImpl.getUserInfoByToken(token);
		if(callback!=null&&!callback.equals("")){
			return callback +"(" + JSON.toJSONString(er) +")";
		}
		return er;
	}
	
	@RequestMapping("user/logout/{token}")
	@ResponseBody
	public Object logout(@PathVariable String token, String callback, HttpServletRequest req, HttpServletResponse res){
		EgoResult er = tbUserServiceImpl.logout(token, req, res);
		if(callback!=null&&!callback.equals("")){
			return callback +"(" + JSON.toJSONString(er) +")";
		}
		return er;
	}
}
