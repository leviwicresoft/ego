package com.levi.cart.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.levi.commons.pojo.CookieUtil;
import com.levi.commons.pojo.EgoResult;
import com.levi.commons.pojo.HttpClientUtil;

public class CartInterceptor implements HandlerInterceptor{
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String token = CookieUtil.getCookie(request, response, "TT_TOKEN");
		if(token!=null && !token.equals("")){
			String re = HttpClientUtil.doGet("http://localhost:8084/user/token/"+token);
			EgoResult er = JSON.parseObject(re, EgoResult.class);
			if(er.getStatus() == 200){
				return true;
			}
		}
			String num = request.getParameter("num");
			response.sendRedirect("http://localhost:8084/user/showLogin?interUrl=" + request.getRequestURL()+"%3Fnum=" +num);
		
		return false;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
	}

}
