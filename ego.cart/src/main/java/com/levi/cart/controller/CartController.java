package com.levi.cart.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.levi.cart.service.TbCartService;
import com.levi.commons.pojo.EgoResult;

@Controller
public class CartController {
	@Resource
	private TbCartService tbCartServiceImpl;
	
	@RequestMapping("cart/add/{id}.html")
	public String addCart(@PathVariable long id, int num, HttpServletRequest req, HttpServletResponse res){
		try {
			tbCartServiceImpl.addCart(id, num, req, res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "cartSuccess";
	}

	@RequestMapping("cart/cart.html")
	public String showCart(HttpServletRequest req, HttpServletResponse res,Model model){
		model.addAttribute("carList", tbCartServiceImpl.showCart(req, res));
		return "cart";
	}
	
	@RequestMapping("cart/update/{id}/{num}.action")
	@ResponseBody
	public EgoResult update(@PathVariable long id, @PathVariable int num, HttpServletRequest req, HttpServletResponse res){
		return tbCartServiceImpl.update(id, num, req, res);
	}
	
	@RequestMapping("cart/delete/{id}.action")
	@ResponseBody
	public EgoResult update(@PathVariable long id, HttpServletRequest req, HttpServletResponse res){
		return tbCartServiceImpl.delete(id, req, res);
	}
	
}
