package com.levi.cart.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.levi.cart.service.TbCartService;
import com.levi.commons.pojo.CookieUtil;
import com.levi.commons.pojo.EgoResult;
import com.levi.commons.pojo.HttpClientUtil;
import com.levi.commons.pojo.TbItemChild;
import com.levi.dubbo.service.TbItemDubboService;
import com.levi.pojo.TbItem;
import com.levi.pojo.TbUser;
import com.levi.redis.dao.RedisDao;
@Service
public class TbCartServiceImpl implements TbCartService{
	@Resource
	private TbItemDubboService tbItemDubboServiceImpl;
	@Resource
	private RedisDao redisDaoImpl;
	
	@Value("${passport.url}")
	private String passportUrl;
	
	@Value("${cart.user}")
	private String cartUser;
	
	
	public void addCart(long id, int num, HttpServletRequest req, HttpServletResponse res) throws Exception {
		String token = CookieUtil.getCookie(req, res, "TT_TOKEN");
		EgoResult er = JSON.parseObject(HttpClientUtil.doGet(passportUrl + token), EgoResult.class);
		String key = cartUser + ((TbUser)er.getData()).getUsername();
		//存放购物车商品
		List<TbItemChild> list = new ArrayList();
		if(redisDaoImpl.exists(key)){
			String json = redisDaoImpl.get(key);
		if(json != null &&!json.equals("")){
			list = JSON.parseArray(json, TbItemChild.class);
			for(TbItemChild child :list){
				if(child.getId() == id){
					child.setNum(child.getNum() + num);
					redisDaoImpl.set(key, JSON.toJSONString(list));
					return;
				}
			}
		}
		}
		TbItemChild itemChild = new TbItemChild();
		TbItem item = tbItemDubboServiceImpl.selById(id);
		
		itemChild.setId(item.getId());
		itemChild.setTitle(item.getTitle());
		itemChild.setImages(item.getImage()==null || item.getImage().equals("")?new String[1]:item.getImage().split(","));
		itemChild.setNum(num);
		itemChild.setPrice(item.getPrice());
		
		list.add(itemChild);
		redisDaoImpl.set(key, JSON.toJSONString(list));
		
	}


	public List<TbItemChild> showCart(HttpServletRequest req, HttpServletResponse res) {
		String token = CookieUtil.getCookie(req, res, "TT_TOKEN");
		EgoResult er = new EgoResult();
		try {
			er = JSON.parseObject(HttpClientUtil.doGet(passportUrl + token), EgoResult.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String key = cartUser + ((TbUser)er.getData()).getUsername();
		
		return JSON.parseArray(redisDaoImpl.get(key), TbItemChild.class);
	}


	public EgoResult update(long id, int num, HttpServletRequest req, HttpServletResponse res) {
		String token = CookieUtil.getCookie(req, res, "TT_TOKEN");
		EgoResult er = new EgoResult();
		try {
			er = JSON.parseObject(HttpClientUtil.doGet(passportUrl + token), EgoResult.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String key = cartUser + ((TbUser)er.getData()).getUsername();
		
		EgoResult result = new EgoResult();
		List<TbItemChild> list= JSON.parseArray(redisDaoImpl.get(key), TbItemChild.class);
		for(TbItemChild child : list){
			if(child.getId()==id){
				child.setNum(num);
				String ok = redisDaoImpl.set(key, JSON.toJSONString(list));
				if(ok=="OK"){
					result.setStatus(200);;
				}
			}
		}
		return result;
	}


	public EgoResult delete(long id, HttpServletRequest req, HttpServletResponse res) {
		String token = CookieUtil.getCookie(req, res, "TT_TOKEN");
		EgoResult er = new EgoResult();
		try {
			er = JSON.parseObject(HttpClientUtil.doGet(passportUrl + token), EgoResult.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String key = cartUser + ((TbUser)er.getData()).getUsername();
		List<TbItemChild> list= JSON.parseArray(redisDaoImpl.get(key), TbItemChild.class);
		TbItemChild item = new TbItemChild();
		for(TbItemChild child : list){
			if(child.getId()==id){
				item = child;
				}
			}
		list.remove(item);
		String ok = redisDaoImpl.set(key, JSON.toJSONString(list));
		EgoResult result = new EgoResult();
		if(ok=="OK"){
			result.setStatus(200);;
		}
		return result;
	}

}
