package com.levi.cart.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.levi.commons.pojo.EgoResult;
import com.levi.commons.pojo.TbItemChild;

public interface TbCartService {
	
	void addCart(long id, int num, HttpServletRequest req, HttpServletResponse res) throws Exception;
	
	List<TbItemChild> showCart(HttpServletRequest req, HttpServletResponse res);
	
	EgoResult update(long id, int num , HttpServletRequest req, HttpServletResponse res);
	
	EgoResult delete(long id, HttpServletRequest req, HttpServletResponse res);
}
