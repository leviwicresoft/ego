package com.levi.manager.pojo;

import com.levi.pojo.TbItemParam;

//封装前端jsp中显示的数据内容
public class TbItemParamChild extends TbItemParam{

	private String itemCatName;

	public String getItemCatName() {
		return itemCatName;
	}

	public void setItemCatName(String itemCatName) {
		this.itemCatName = itemCatName;
	}
	
	
}
