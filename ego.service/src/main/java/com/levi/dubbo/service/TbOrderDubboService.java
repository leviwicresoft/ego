package com.levi.dubbo.service;

import java.util.List;

import com.levi.pojo.TbOrder;
import com.levi.pojo.TbOrderItem;
import com.levi.pojo.TbOrderShipping;

public interface TbOrderDubboService {

	int insOrder(TbOrder order, List<TbOrderItem> list, TbOrderShipping shipping ) throws Exception;
}
