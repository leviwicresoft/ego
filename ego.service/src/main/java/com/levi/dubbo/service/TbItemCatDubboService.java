package com.levi.dubbo.service;

import java.util.List;

import com.levi.pojo.TbItemCat;

public interface TbItemCatDubboService {
	
	List<TbItemCat> show(long pid);
	TbItemCat selById(long id);
}
