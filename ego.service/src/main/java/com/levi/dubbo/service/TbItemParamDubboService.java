package com.levi.dubbo.service;

import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.pojo.TbItemParam;

public interface TbItemParamDubboService {

	EasyUIDataGrid showPage(int page, int rows);
	
	int delByIds(String ids) throws Exception;
	/*
	 * 根据类目id 查询模板
	 */
	TbItemParam selByCatId(long catId);
	
	/*
	 * 增加类目模板
	 */
	int insParam(TbItemParam param);
	
}
