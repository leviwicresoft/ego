package com.levi.dubbo.service;

import java.util.List;

import com.levi.pojo.TbContentCategory;

public interface TbContentCategoryDubboService {
	
	//根据父类目查询
	List<TbContentCategory> selByPid(long id);
	
	//新增
	int insTbContentCategory(TbContentCategory cate);
	
	//新增后修改isparent
	int updIsParentById(TbContentCategory cate);
	
	//通过id查询商品详细信息
	TbContentCategory selById(long id);
}
