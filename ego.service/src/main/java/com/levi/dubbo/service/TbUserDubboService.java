package com.levi.dubbo.service;

import com.levi.pojo.TbUser;

public interface TbUserDubboService {
	TbUser selByTbUser(TbUser tbUser);
}
