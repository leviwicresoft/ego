package com.levi.dubbo.service;

import java.util.List;

import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.pojo.TbContent;

public interface TbContentDubboService {
	//查询内容
	EasyUIDataGrid selContentById(long categoryId, int page, int rows);

	//新增内容
	int insContent(TbContent content);
	
	List<TbContent> selByCount(int count, boolean isSort);
}
