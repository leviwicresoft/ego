package com.levi.dubbo.service;

import java.util.List;

import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.pojo.TbItem;
import com.levi.pojo.TbItemDesc;
import com.levi.pojo.TbItemParamItem;

public interface  TbItemDubboService {
	EasyUIDataGrid show(int page, int rows);
	
	int updItemStatus(TbItem tbItem);
	
	int insTbItem(TbItem tbItem);
	
	/*
	 * 新增商品表和商品描述表
	 */
	int insTbItemDesc(TbItem tbItem, TbItemDesc tbItemDesc, TbItemParamItem paramItems) throws Exception;
	
	List<TbItem> selAllByStatus(byte status);
	
	TbItem selById(long id);
}
