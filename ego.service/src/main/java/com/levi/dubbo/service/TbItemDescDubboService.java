package com.levi.dubbo.service;

import com.levi.pojo.TbItemDesc;

public interface TbItemDescDubboService {

	int insTbItemDesc(TbItemDesc tbItemDesc);
	
	TbItemDesc selByItemId(long id);
}
