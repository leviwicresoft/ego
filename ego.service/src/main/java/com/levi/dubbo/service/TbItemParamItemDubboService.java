package com.levi.dubbo.service;

import com.levi.pojo.TbItemParamItem;

public interface TbItemParamItemDubboService {
	TbItemParamItem selByItemId(long id);
}
