package com.levi.manage.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.levi.commons.pojo.EasyUITree;
import com.levi.manage.service.TbItemCatService;
@Controller
public class TbItemCatController {
	
	@Autowired
	private TbItemCatService tbItemCatServiceImpl;

	@RequestMapping("item/cat/list")
	@ResponseBody
	public List<EasyUITree> showCat(@RequestParam(defaultValue ="0") long id){
		return tbItemCatServiceImpl.show(id);
	}
}
