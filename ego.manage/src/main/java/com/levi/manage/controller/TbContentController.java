package com.levi.manage.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.commons.pojo.EgoResult;
import com.levi.manage.service.TbContentService;
import com.levi.pojo.TbContent;

@Controller
public class TbContentController {
	
	@Resource
	private TbContentService tbContentServiceImpl;
	
	@RequestMapping("content/query/list")
	@ResponseBody
	public EasyUIDataGrid showContent(long categoryId, int page, int rows){
		return tbContentServiceImpl.showContent(categoryId, page, rows);
	}
	
	@RequestMapping("content/save")
	@ResponseBody
	public EgoResult save(TbContent content){
		return tbContentServiceImpl.save(content);
	}

}
