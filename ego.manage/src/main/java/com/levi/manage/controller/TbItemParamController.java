package com.levi.manage.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.commons.pojo.EgoResult;
import com.levi.manage.service.TbItemParamService;
import com.levi.pojo.TbItemParam;

@Controller
public class TbItemParamController {

	@Resource
	private TbItemParamService tbItemParamServiceImpl;
	
	@RequestMapping("/item/param/list")
	@ResponseBody
	public EasyUIDataGrid showPage(int page, int rows){
		return tbItemParamServiceImpl.showPage(page, rows);
	}
	
	@RequestMapping("/item/param/delete")
	@ResponseBody
	public EgoResult delete(String ids){
		EgoResult result = new EgoResult();
		
		try {
			int index = tbItemParamServiceImpl.delete(ids);
			if(index ==1){
				result.setStatus(200);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.setData(e.getMessage());
		}
		return result;
	}
	/*
	 * 查看商品类目按钮
	 * 判断类目是否已经添加模板
	 */
	@RequestMapping("item/param/query/itemcatid/{catId}")
	@ResponseBody
	public EgoResult show(@PathVariable long catId){
		return tbItemParamServiceImpl.showParam(catId);
	}
	
	/*
	 * 商品类目新增
	 */
	
	@RequestMapping("/item/param/save/{catId}")
	@ResponseBody
	public EgoResult save(TbItemParam param, @PathVariable long catId){
		param.setItemCatId(catId);
		return tbItemParamServiceImpl.save(param);
	}
}
