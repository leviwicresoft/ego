package com.levi.manage.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.levi.manage.service.PicService;

@Controller
public class PicController {
	
@Resource
private PicService picServiceImpl;

/**
 * picture upload
 * @param uploadFile
 * @return
 */

@RequestMapping("/pic/upload")
@ResponseBody
public Map<String, Object> upload(MultipartFile uploadFile){
	
		Map<String, Object> map = new HashMap();;
		try { 
			map =  picServiceImpl.upload(uploadFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			map.put("error", 2);
			map.put("message", "error occured");
			
		}
	return map;
	
}
	
}
