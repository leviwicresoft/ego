package com.levi.manage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.commons.pojo.EgoResult;
import com.levi.manage.service.TbItemService;
import com.levi.pojo.TbItem;

@Controller
public class TbItemController {
	@Autowired
	private TbItemService tbItemServiceImpl;
	
	@RequestMapping(value="/{page}")
	public String index(@PathVariable("page") String page){
		return page;
	}
	@RequestMapping("item/list")
	@ResponseBody
	public EasyUIDataGrid show(int page, int rows){
		return tbItemServiceImpl.show(page, rows);
	}
	
	@RequestMapping("rest/page/item-edit")
	public String showItemEdit(){
		return "item-edit";
	}
	
	@RequestMapping("rest/item/delete")
	public EgoResult delete(String ids){
		EgoResult result = new EgoResult();
		int index = tbItemServiceImpl.update(ids, (byte)3);
		if(index == 1){
			result.setStatus(200);
		}
		return result;
	}
	
	//上架
	@RequestMapping("rest/item/reshelf")
	public EgoResult reshelf(String ids){
		EgoResult result = new EgoResult();
		int index = tbItemServiceImpl.update(ids, (byte)1);
		if(index == 1){
			result.setStatus(200);
		}
		return result;
	}
	
	//下架
	@RequestMapping("rest/item/instock")
	public EgoResult instock(String ids){
		EgoResult result = new EgoResult();
		int index = tbItemServiceImpl.update(ids, (byte)2);
		if(index == 1){
			result.setStatus(200);
		}
		return result;
	}
	
	//商品新增
	@RequestMapping("ego.manage/item/save")
	@ResponseBody
	public EgoResult insert(TbItem item, String desc, String itemParams){
		EgoResult er = new EgoResult();
		int index = 0;
		try {
			index = tbItemServiceImpl.save(item, desc, itemParams);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(index == 1){
			er.setStatus(200);
		}
		return er;
	}
}
