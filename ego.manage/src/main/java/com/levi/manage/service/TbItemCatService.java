package com.levi.manage.service;

import java.util.List;

import com.levi.commons.pojo.EasyUITree;

public interface TbItemCatService {
List<EasyUITree> show(long pid);
}
