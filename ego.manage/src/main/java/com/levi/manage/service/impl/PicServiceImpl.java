package com.levi.manage.service.impl;

import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.levi.manage.service.PicService;

@Service
public class PicServiceImpl implements PicService {

	@Value("${ftpclient.host}")
	private String host;
	@Value("${ftpclient.port}")
	private int port;
	@Value("${ftpclient.username}")
	private String username;
	@Value("${ftpclient.password}")
	private String password;
	@Value("${ftpclient.basepath}")
	private String basepath;
	@Value("${ftpclient.filepath")
	private String filepath;
	
	public Map<String, Object> upload(MultipartFile file) throws Exception{
		FTPClient ftpClient = new FTPClient();
		ftpClient.connect(host, port);
		ftpClient.login(username, password);
		ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
		ftpClient.changeWorkingDirectory(basepath);
		String fileName = UUID.randomUUID() + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		boolean result = ftpClient.storeFile(fileName,file.getInputStream());
		ftpClient.disconnect();
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String,Object>();
		
		if(result){
			map.put("error", 0);
			map.put("url", "http://" + host + "/" + fileName);
		}else{
			map.put("error", 1);
			map.put("status", "upload falied");
		}
		return map;
	}

}
