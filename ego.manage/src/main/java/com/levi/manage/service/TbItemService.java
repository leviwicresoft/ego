package com.levi.manage.service;

import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.pojo.TbItem;

public interface TbItemService {
public EasyUIDataGrid show(int page, int rows);
int update(String ids, byte status);
int save(TbItem tbItem, String desc, String itemParams) throws Exception;
}
