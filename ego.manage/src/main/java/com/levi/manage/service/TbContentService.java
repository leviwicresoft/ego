package com.levi.manage.service;

import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.commons.pojo.EgoResult;
import com.levi.pojo.TbContent;

public interface TbContentService {
	//分页显示内容信息
	
	EasyUIDataGrid showContent(long categoryId, int page, int rows);
	
	EgoResult save(TbContent content);

}
