package com.levi.manage.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jackrabbit.uuid.UUID;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.levi.commons.pojo.EasyUITree;
import com.levi.commons.pojo.EgoResult;
import com.levi.dubbo.service.TbContentCategoryDubboService;
import com.levi.manage.service.TbContentCategoryService;
import com.levi.pojo.TbContentCategory;

@Service
public class TbContentCategoryServiceImpl implements TbContentCategoryService{

	@Reference
	private TbContentCategoryDubboService tbContentCategoryDubboServiceImpl;
	
	public List<EasyUITree> showCategory(long id) {
		List<EasyUITree> listTree = new ArrayList<EasyUITree>();
		
		List<TbContentCategory> list = tbContentCategoryDubboServiceImpl.selByPid(id);
		for(TbContentCategory tcc : list){
			EasyUITree tree = new EasyUITree();
			tree.setId(tcc.getId());
			tree.setText(tcc.getName());
			tree.setState(tcc.getIsParent()?"closed":"open");
			
			listTree.add(tree);
		}
		return listTree;
	}

	public EgoResult create(TbContentCategory cate) {
		EgoResult er = new EgoResult();
		List<TbContentCategory> listChild = tbContentCategoryDubboServiceImpl.selByPid(cate.getParentId());
		for(TbContentCategory child : listChild){
			if(cate.getName() == child.getName()){
				er.setData("类目已经存在！！");
				return er;
			}
		}
		
		Date date = new Date();
		cate.setCreated(date);
		cate.setUpdated(date);
		cate.setStatus(1);
		cate.setIsParent(false);
		cate.setSortOrder(1);
		
		long id =Long.parseLong(UUID.randomUUID().toString());
		cate.setId(id);
		
		int index = tbContentCategoryDubboServiceImpl.insTbContentCategory(cate);
		if(index>0){
			TbContentCategory parent = new TbContentCategory();
			parent.setId(id);
			parent.setIsParent(true);
			tbContentCategoryDubboServiceImpl.updIsParentById(parent);
			
			er.setStatus(200);
			Map<String, Long> map = new HashMap<String, Long>();
			map.put("id", id);
			er.setData(map);
			
		}
		return er;
	}

	public EgoResult update(TbContentCategory cate) {
		EgoResult er = new EgoResult();
		TbContentCategory cateSelect = tbContentCategoryDubboServiceImpl.selById(cate.getId());
		List<TbContentCategory> list = tbContentCategoryDubboServiceImpl.selByPid(cate.getParentId());
		for(TbContentCategory child : list){
			if(child.getName() == cateSelect.getName()){
				er.setData("分类已经存在");
				return er;
			}
		}
		int index = tbContentCategoryDubboServiceImpl.updIsParentById(cate);
		if(index>0){
			er.setStatus(200);
		}
		return er;
	}

	public EgoResult delete(TbContentCategory cate) {
		EgoResult er = new EgoResult();
		cate.setStatus(0);
		int index = tbContentCategoryDubboServiceImpl.updIsParentById(cate);
		if(index>0){
			TbContentCategory tcc = tbContentCategoryDubboServiceImpl.selById(cate.getId());
			List<TbContentCategory> list = tbContentCategoryDubboServiceImpl.selByPid(tcc.getParentId());
			if(list == null || list.size()==0){
				TbContentCategory parent = new TbContentCategory();
				parent.setId(tcc.getParentId());
				parent.setIsParent(false);
				int result = tbContentCategoryDubboServiceImpl.updIsParentById(parent);
				if(result>0){
					er.setStatus(200);
				}
			}else{
				er.setStatus(200);
			}
		}
		return er;
	}
}
