package com.levi.manage.service;

import java.util.List;

import com.levi.commons.pojo.EasyUITree;
import com.levi.commons.pojo.EgoResult;
import com.levi.pojo.TbContentCategory;

public interface TbContentCategoryService {

	List<EasyUITree> showCategory(long id);
	
	EgoResult create(TbContentCategory cate);
	
	EgoResult update(TbContentCategory cate);
	
	EgoResult delete(TbContentCategory cate);
}
