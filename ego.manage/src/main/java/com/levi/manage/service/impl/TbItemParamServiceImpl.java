package com.levi.manage.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.commons.pojo.EgoResult;
import com.levi.dubbo.service.TbItemCatDubboService;
import com.levi.dubbo.service.TbItemParamDubboService;
import com.levi.manage.service.TbItemParamService;
import com.levi.manager.pojo.TbItemParamChild;
import com.levi.pojo.TbItemParam;

@Service
public class TbItemParamServiceImpl implements TbItemParamService{

	@Reference
	private TbItemParamDubboService tbItemParamDubboServiceImpl;
	
	@Reference
	private TbItemCatDubboService tbItemCatDubboServiceImpl;
	
	public EasyUIDataGrid showPage(int page, int rows) {
		
		EasyUIDataGrid dataGrid = tbItemParamDubboServiceImpl.showPage(page, rows);
		List<TbItemParam> listPara = (List<TbItemParam>)dataGrid.getRows();
		List<TbItemParamChild> listChild = new ArrayList<TbItemParamChild>();
		for(TbItemParam para: listPara){
			TbItemParamChild child = new TbItemParamChild();
			child.setCreated(para.getCreated());
			child.setId(para.getId());
			child.setItemCatId(para.getItemCatId());
			child.setUpdated(para.getUpdated());
			child.setItemCatName(tbItemCatDubboServiceImpl.selById(child.getItemCatId()).getName());
			listChild.add(child);
		}
		dataGrid.setRows(listChild);
		return dataGrid;
	}
	
	public int delete(String ids) throws Exception{
		return tbItemParamDubboServiceImpl.delByIds(ids);
	}

	public EgoResult showParam(long catId) {
		EgoResult er = new EgoResult();
		TbItemParam param = tbItemParamDubboServiceImpl.selByCatId(catId);
		if(param != null){
			er.setStatus(200);
			er.setData(param);
		}
		return er;
	}

	public EgoResult save(TbItemParam param) {
	Date date = new Date();
	param.setCreated(date);
	param.setUpdated(date);
	int index = tbItemParamDubboServiceImpl.insParam(param);
	EgoResult er = new EgoResult();
	if(index>0){
		er.setStatus(200);
	}
	return er;
	}
}
