package com.levi.manage.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.dubbo.common.json.JSON;
import com.alibaba.dubbo.common.json.ParseException;
import com.alibaba.dubbo.config.annotation.Reference;
import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.commons.pojo.EgoResult;
import com.levi.dubbo.service.TbContentDubboService;
import com.levi.manage.service.TbContentService;
import com.levi.pojo.TbContent;
import com.levi.redis.dao.RedisDao;

@Service
public class TbContentServiceImpl implements TbContentService{

	@Reference
	private TbContentDubboService tbContentDubboServiceImpl;
	@Resource
	private RedisDao redisDaoImpl;
	
	public EasyUIDataGrid showContent(long categoryId, int page, int rows) {
		return tbContentDubboServiceImpl.selContentById(categoryId, page, rows);
	}
	
	public EgoResult save(TbContent content){
		
		String value = redisDaoImpl.get("bigPic");
		EgoResult er = new EgoResult();
		Date date = new Date();
		content.setCreated(date);
		content.setUpdated(date);
		int index = tbContentDubboServiceImpl.insContent(content);
				
		if(value != null && !value.equals("")){
			try {
				List<HashMap> list = (List<HashMap>) JSON.parse(value, HashMap.class);
						
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("srcB",content.getPic2());
				map.put("height",240);
				map.put("alt", "对不起 加载失败");
				map.put("widthB", 550);
				map.put("href", content.getUrl());
				map.put("heightB", 240);
				if(list.size()==6){
					list.remove(5);
				}
				list.add(0,(HashMap) map);
				try {
					redisDaoImpl.set("bigPic",JSON.json(list));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(index >0){
			er.setStatus(200);
		}
							
		return er;
	}
}
