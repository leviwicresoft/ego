package com.levi.manage.service;

import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.commons.pojo.EgoResult;
import com.levi.pojo.TbItemParam;

public interface TbItemParamService {
	EasyUIDataGrid showPage(int page, int rows);
	
	int delete(String ids) throws Exception;
	
	/*
	 * 根据商品类目id 查询模板
	 */
	EgoResult showParam(long catId);
	/*
	 * 新增模板
	 */
	EgoResult save(TbItemParam param);
}
