package com.levi.manage.service;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

public interface PicService {
	
public Map<String, Object> upload(MultipartFile file) throws Exception;
}
