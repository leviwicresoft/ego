package com.levi.manage.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.levi.commons.pojo.EasyUITree;
import com.levi.dubbo.service.TbItemCatDubboService;
import com.levi.manage.service.TbItemCatService;
import com.levi.pojo.TbItemCat;

@Service
public class TbItemCatServiceImpl implements TbItemCatService{
	
	@Reference
	private TbItemCatDubboService tbItemCatDubboServiceImpl;
	
	public List<EasyUITree> show(long pid) {
		List<TbItemCat> list = tbItemCatDubboServiceImpl.show(pid);
		List<EasyUITree> listTree = new ArrayList<EasyUITree>();
		for(TbItemCat cat:list){
			EasyUITree tree = new EasyUITree();
			tree.setId(cat.getId());
			tree.setText(cat.getName());
			tree.setState(cat.getIsParent()?"closed":"open");
			listTree.add(tree);
		}
		return listTree;
	}

	
}
