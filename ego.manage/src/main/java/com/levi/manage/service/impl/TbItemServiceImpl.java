package com.levi.manage.service.impl;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.levi.commons.pojo.EasyUIDataGrid;
import com.levi.commons.pojo.HttpClientUtil;
import com.levi.dubbo.service.TbItemDescDubboService;
import com.levi.dubbo.service.TbItemDubboService;
import com.levi.manage.service.TbItemService;
import com.levi.pojo.TbItem;
import com.levi.pojo.TbItemDesc;
import com.levi.pojo.TbItemParamItem;
import com.levi.redis.dao.RedisDao;

@Service
public class TbItemServiceImpl implements TbItemService {
	@Reference
	private TbItemDubboService tbItemDubboServiceImpl;
	
	@Reference
	private TbItemDescDubboService tbItemDescDubboServiceImpl;
	
	@Resource
	private RedisDao redisDaoImpl;
	
	@Value("${solrServer.url}")
	private String solrUrl;
	
	@Value("${redis.item.key}")
	private String itemKey;
	
	public EasyUIDataGrid show(int page, int rows) {
		return tbItemDubboServiceImpl.show(page, rows);
		
	}

	public int update(String ids, byte status) {
		int index = 0;
		TbItem tbItem = new TbItem();
		String[] idsStr = ids.split(",");
		for(String id: idsStr){
			tbItem.setId(Long.parseLong(id));
			tbItem.setStatus(status);
			index += tbItemDubboServiceImpl.updItemStatus(tbItem);
			
			//从redis 删除下架缓存
			if(status ==2|| status==3){
				redisDaoImpl.del(itemKey+id);
			}
			
		}
		if(index == idsStr.length){
			return 1;
		}
		
		return 0;
	}
	//商品新增
	public int save(TbItem tbItem, String desc, String itemParams) throws Exception{
		
		long id = Long.parseLong(UUID.randomUUID().toString());
		tbItem.setId(id);
		Date date = new Date();
		tbItem.setCreated(date);
		tbItem.setUpdated(date);
		tbItem.setStatus((byte)1);
		
		TbItemDesc itemDesc = new TbItemDesc();
		itemDesc.setItemDesc(desc);
		itemDesc.setItemId(id);
		itemDesc.setCreated(date);
		itemDesc.setUpdated(date);
		
		TbItemParamItem paramItems = new TbItemParamItem();
		paramItems.setCreated(date);
		paramItems.setUpdated(date);
		paramItems.setItemId(id);
		paramItems.setParamData(itemParams);
		int index = 0;
		index = tbItemDubboServiceImpl.insTbItemDesc(tbItem, itemDesc, paramItems);
			
		//同步solr 搜索库
		final TbItem tbItemFinal = tbItem;
		final String descFinal = desc; 
			
		new Thread(){
			public void run(){
				Map<String, Object> map = new HashMap();
				map.put("item", tbItemFinal);
				map.put("desc", descFinal);
				try {
					HttpClientUtil.doPostJson(solrUrl, map);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}}.start();
		
		return index;
	}
	
}
