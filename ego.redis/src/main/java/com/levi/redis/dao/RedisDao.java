package com.levi.redis.dao;

public interface RedisDao {
	
	boolean exists(String key);
	
	long del(String key);
	
	String set(String key, String value);
	
	String get(String key);
	
	long expire(String key, int maxAge);
}
