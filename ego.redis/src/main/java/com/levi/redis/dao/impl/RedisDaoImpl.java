package com.levi.redis.dao.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.levi.redis.dao.RedisDao;

import redis.clients.jedis.JedisCluster;

@Repository
public class RedisDaoImpl implements RedisDao{

	@Resource
	private JedisCluster jedisCluster;
	
	public boolean exists(String key) {
		// TODO Auto-generated method stub
		return jedisCluster.exists(key);
	}

	public long del(String key) {
		// TODO Auto-generated method stub
		return jedisCluster.del(key);
	}

	public String set(String key, String value) {
		// TODO Auto-generated method stub
		return jedisCluster.set(key, value);
	}

	public String get(String key) {
		// TODO Auto-generated method stub
		return jedisCluster.get(key);
	}

	public long expire(String key, int maxAge) {
		return jedisCluster.expire(key, maxAge);
	}

}
