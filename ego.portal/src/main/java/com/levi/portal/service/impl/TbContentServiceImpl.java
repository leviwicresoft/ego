package com.levi.portal.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.alibaba.dubbo.common.json.JSON;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.levi.dubbo.service.TbContentDubboService;
import com.levi.pojo.TbContent;
import com.levi.portal.service.TbContentService;
import com.levi.redis.dao.RedisDao;

@Service
public class TbContentServiceImpl implements TbContentService{
	
	@Reference
	private TbContentDubboService tbContentDubboServiceImpl;
	@Resource
	private RedisDao redisDaoImpl;

	public String showBigPic() {
		if(redisDaoImpl.exists("bigPic")){
			String value = redisDaoImpl.get("bigPic");
			if(value != null && !value.equals("")){
				return value;
			}
			
		}
		List<TbContent> list = tbContentDubboServiceImpl.selByCount(6, true);
		List<Map<String,Object>> listResult = new ArrayList<Map<String, Object>>();
		for(TbContent content: list){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("srcB",content.getPic2());
			map.put("height",240);
			map.put("alt", "对不起 加载失败");
			map.put("widthB", 550);
			map.put("href", content.getUrl());
			map.put("heightB", 240);
			listResult.add(map);
		}
		String string = null;
		try {
			string = JSON.json(listResult);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		redisDaoImpl.set("bigPic", string);
		return string;
	}
	
	

}
