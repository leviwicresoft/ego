package com.levi.search.controller;

import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.levi.pojo.TbItem;
import com.levi.search.service.TbItemService;

@Controller
public class TbItemController {
	
	@Resource
	private TbItemService tbItemServiceImpl;
	
	@RequestMapping(value="solr/init", produces="text/html;charset=utf-8")
	@ResponseBody
	public String init(){
		try {
			tbItemServiceImpl.init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "init finished";
	}
	
	//搜索功能
	@RequestMapping("search.html")//.html 伪静态提供搜索效率，ajax 不支持
	public String search(Model model,String q, @RequestParam(defaultValue="1")int page, @RequestParam(defaultValue="12")int rows){
		try {
			q = new String(q.getBytes("iso-8859-1"),"utf-8");
		
		Map<String, Object> map = tbItemServiceImpl.selByQuery(q, page, rows);
		model.addAttribute("query", q);
		model.addAttribute("itemList", map.get("itemList"));
		model.addAttribute("totalPages", map.get("totalPages"));
		model.addAttribute("page", page);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "rearch";
	}
	
	@RequestMapping("solr/add")
	@ResponseBody
	public int add(@RequestBody Map<String,Object> map){
		try {
			return tbItemServiceImpl.add((LinkedHashMap)map.get("item"), map.get("desc").toString());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			return 0;
		}
	}
	
}
