package com.levi.search.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.levi.commons.pojo.TbItemChild;
import com.levi.dubbo.service.TbItemCatDubboService;
import com.levi.dubbo.service.TbItemDescDubboService;
import com.levi.dubbo.service.TbItemDubboService;
import com.levi.pojo.TbItem;
import com.levi.pojo.TbItemCat;
import com.levi.pojo.TbItemDesc;

import com.levi.search.service.TbItemService;

@Service
public class TbItemServiceImpl implements TbItemService{

	@Reference
	private TbItemDubboService tbItemDubboServiceImpl;
	@Reference
	private TbItemCatDubboService tbItemCatDubboServiceImpl;
	@Reference
	private TbItemDescDubboService tbItemDescDubboServiceImpl;
	@Resource
	private CloudSolrClient solrjClient;
	//初始化搜索库
	public void init() throws Exception {
					
		List<TbItem> listItem = tbItemDubboServiceImpl.selAllByStatus((byte)1);
		for(TbItem item: listItem){
				
					TbItemCat cat = tbItemCatDubboServiceImpl.selById(item.getCid());
					TbItemDesc desc = tbItemDescDubboServiceImpl.selByItemId(item.getId());
					
					SolrInputDocument doc = new SolrInputDocument();
				try{	
					doc.addField("id",item.getId());
					doc.addField("item_title", item.getSellPoint());
					doc.addField("item_price", item.getPrice());
					doc.addField("item_image", item.getImage());
					doc.addField("item_category_name", cat.getName());
					doc.addField("item_desc", desc.getItemDesc());
				}catch(Exception e){
					e.printStackTrace();
				}
					solrjClient.add(doc);
				
		}
		solrjClient.commit();
	}

	public Map<String, Object> selByQuery(String query, int page, int rows) throws Exception {
		SolrQuery params = new SolrQuery();
		params.setStart(rows*(page-1));
		params.setRows(rows);
		params.setQuery("item_keywords:" + query);
		params.setHighlight(true);
		params.addHighlightField("item_title");
		params.setHighlightSimplePre("<span style='color:red'>");
		params.setHighlightSimplePost("</span>");
		QueryResponse response = solrjClient.query(params);
		
		List<TbItemChild> listChild = new ArrayList<TbItemChild>();
		SolrDocumentList listSolr = response.getResults();
				
		Map<String, Map<String, List<String>>> hh = response.getHighlighting();
		
		for( SolrDocument doc : listSolr){
			TbItemChild child = new TbItemChild();
			child.setId(Long.parseLong(doc.getFieldValue("id").toString()));
			List<String> list= hh.get(doc.getFieldValue("id")).get("item_title");
			if(list!=null&& list.size()>0){
				child.setTitle(list.get(0));
			}else{
				child.setTitle(doc.getFieldValue("item_title").toString());
			}
			child.setPrice((Long)doc.getFieldValue("item_price"));
			Object image = doc.getFieldValue("item_image");
			child.setImages(image==null || image.equals("")? new String[1]:image.toString().split(","));
			child.setSellPoint(doc.getFieldValue("item_sell_point").toString());
			listChild.add(child);		
		}
		Map<String,Object> map = new HashMap();
		map.put("itemList", listChild);
		map.put("totalPages", listSolr.getNumFound()%rows == 0? listSolr.getNumFound()%rows :listSolr.getNumFound()%rows+1);
		return map;
	}
	//solr 中新增
	public int add(Map<String,Object> map, String desc) throws Exception{
		SolrInputDocument doc = new SolrInputDocument();
		
		doc.setField("id",map.get("id"));
		doc.setField("item_title", map.get("title"));
		doc.setField("item_price", map.get("price"));
		doc.setField("item_image", map.get("image"));
		doc.setField("item_category_name", tbItemCatDubboServiceImpl.selById((Integer)map.get("cid")).getName());
		doc.setField("item_desc", desc);
		doc.setField("item_sell_point", map.get("sellpoint"));
		
		UpdateResponse response = solrjClient.add(doc);
		
		solrjClient.commit();
		
		if(response.getStatus() == 0){
			return 1;
		}
		return 0;
	}

}
