package com.levi.search.service;

import java.io.IOException;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServerException;

import com.levi.pojo.TbItem;

public interface TbItemService {

	void init() throws Exception;
	
	//分页查询
	Map<String,Object> selByQuery(String query, int page, int rows) throws Exception;
	
	int add(Map<String,Object> map, String desc)throws Exception;
	
}
