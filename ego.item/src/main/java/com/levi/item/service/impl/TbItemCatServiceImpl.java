package com.levi.item.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.levi.dubbo.service.TbItemCatDubboService;
import com.levi.item.pojo.PortalMenu;
import com.levi.item.pojo.PortalMenuChild;
import com.levi.item.service.TbItemCatService;
import com.levi.pojo.TbItemCat;

@Service
public class TbItemCatServiceImpl implements TbItemCatService{

	@Reference
	private TbItemCatDubboService tbItemCatDubboServiceImpl;
	
	public PortalMenu showCatMenu() {
		List<TbItemCat> list = tbItemCatDubboServiceImpl.show(0);
		PortalMenu portalMenu = new PortalMenu();
		portalMenu.setData(selAllMenu(list));
		return portalMenu;
	}
	
	public List<Object> selAllMenu(List<TbItemCat> list){
		List<Object> listNode= new ArrayList<Object>();
		for(TbItemCat item : list){
			if(item.getIsParent()){
			PortalMenuChild menu = new PortalMenuChild();
			menu.setN("<a href='/products/" + item.getId() + ".html'>" + item.getName() + "</a>");
			menu.setU("/products/" + item.getId() +".html");
			menu.setI(selAllMenu(tbItemCatDubboServiceImpl.show(item.getId())));
			listNode.add(menu);
			}else{
				listNode.add("/products/" + item.getId() + ".html|" + item.getName());
			}	
		}
		return listNode;
	}
}
