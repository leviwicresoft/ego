package com.levi.item.service.impl;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.levi.commons.pojo.TbItemChild;
import com.levi.dubbo.service.TbItemDubboService;
import com.levi.item.service.TbItemService;
import com.levi.pojo.TbItem;
import com.levi.redis.dao.RedisDao;

@Service
public class TbItemServiceImpl implements TbItemService{

	@Reference
	private TbItemDubboService tbItemDubboServiceImpl;
	@Resource
	private RedisDao redisDaoImpl;
	
	@Value("${redis.item.key}")
	private String itemKey;
	
	public TbItemChild show(long id) {
		
		String key = itemKey + id;
		if(redisDaoImpl.exists(key)){
			String json = redisDaoImpl.get(key);
			if(json!=null && !json.equals("")){
				return JSON.parseObject(json,TbItemChild.class);
			}
		}
		// TODO Auto-generated method stub
		TbItem item = tbItemDubboServiceImpl.selById(id);
		TbItemChild child = new TbItemChild();
		child.setId(item.getId());
		child.setImages(item.getImage()!=null && !item.equals("")? item.getImage().split("," ): new String[1]);
		child.setPrice(item.getPrice());
		child.setSellPoint(item.getSellPoint());
		child.setTitle(item.getTitle());
		//存到redis缓存
		redisDaoImpl.set(key, JSON.toJSONString(child));
		
		return child;
	}

}
