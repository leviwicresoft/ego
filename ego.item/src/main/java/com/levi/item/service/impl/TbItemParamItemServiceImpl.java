package com.levi.item.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.levi.dubbo.service.TbItemParamItemDubboService;
import com.levi.item.pojo.ParamItem;
import com.levi.item.service.TbItemParamItemService;
import com.levi.redis.dao.RedisDao;

@Service
public class TbItemParamItemServiceImpl implements TbItemParamItemService{
	@Resource
	RedisDao redisDaoImpl;
	
	@Value("${redis.param.key}")
	private String paramKey;
	
	@Reference
	private TbItemParamItemDubboService tbItemParamItemDubboServiceImpl;
	public String showParam(long id) {
		String key = paramKey + id;
		if(redisDaoImpl.exists(key)){
			String param = redisDaoImpl.get(key);
			if(param!= null && !param.equals("")){
				return param;
			}
		}
		String paramData = tbItemParamItemDubboServiceImpl.selByItemId(id).getParamData();
		List<ParamItem> paramItems = JSON.parseArray(paramData, ParamItem.class);
		StringBuilder strb = new StringBuilder();
		for(ParamItem item:paramItems){
			strb.append("<table width='500'>");
			for(int i=0; i< item.getParams().size(); i++){
				if(i==0){
					strb.append("<tr>");
					strb.append("<td align='right' width='30%'>"+item.getGroup()+"</td>");
					strb.append("<td align='right' width='30%'>"+item.getParams().get(i).getK()+"</td>");
					strb.append("<td>"+item.getParams().get(i).getV()+"</td>");
					strb.append("</tr>");
				}
				strb.append("<tr>");
				strb.append("<td></td>");
				strb.append("<td align='right'>"+item.getParams().get(i).getK()+"</td>");
				strb.append("<td align='right'>"+item.getParams().get(i).getV()+"</td>");
				strb.append("</tr>");
			}
			strb.append("</table>");
			strb.append("<hr style = 'color:gray' />");
		}
		redisDaoImpl.set(key,strb.toString());
		return strb.toString();
	}

}
