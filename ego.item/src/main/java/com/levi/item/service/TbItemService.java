package com.levi.item.service;

import com.levi.commons.pojo.TbItemChild;

public interface TbItemService {
	
	TbItemChild show(long id);
}
