package com.levi.item.service.impl;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.levi.dubbo.service.TbItemDescDubboService;
import com.levi.item.service.TbItemDescService;
import com.levi.redis.dao.RedisDao;
@Service
public class TbItemDescServiceImpl implements TbItemDescService {
	@Reference
	private TbItemDescDubboService tbItemDescDubboServiceImpl;
	
	@Value("${redis.desc.key}")
	private String descKey;
	
	@Resource
	private RedisDao redisDaoImpl;
	
	public String showDesc(long id) {
		String key = descKey + id;
		if(redisDaoImpl.exists(key)){
			String json = redisDaoImpl.get(key);
			if(json!=null&&!json.equals("")){
				return json;
			}
		}
		String desc = tbItemDescDubboServiceImpl.selByItemId(id).getItemDesc();
		redisDaoImpl.set(key, desc);
		return desc;
	}

}
