package com.levi.item.pojo;

import java.util.List;

public class PortalMenuChild {
	
	private String n;
	private String u;
	
	private List<Object> i;

	public String getN() {
		return n;
	}

	public void setN(String n) {
		this.n = n;
	}

	public String getU() {
		return u;
	}

	public void setU(String u) {
		this.u = u;
	}

	public List<Object> getI() {
		return i;
	}

	public void setI(List<Object> i) {
		this.i = i;
	}
	
}
