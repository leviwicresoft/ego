package com.levi.item.pojo;

import java.util.List;

public class ParamItem {
	private String group;
	private List<ParamKV> params;
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public List<ParamKV> getParams() {
		return params;
	}
	public void setParams(List<ParamKV> params) {
		this.params = params;
	}
	
}
