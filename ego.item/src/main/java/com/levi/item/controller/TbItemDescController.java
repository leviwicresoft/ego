package com.levi.item.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.levi.item.service.TbItemDescService;

@Controller
public class TbItemDescController {
	@Resource
	private TbItemDescService TbItemDescServiceImpl;
	
	@RequestMapping(value="item/desc/{id}.html", produces="text/html;charset=utf-8")
	@ResponseBody
	public String showDesc(@PathVariable long id){
		
		return TbItemDescServiceImpl.showDesc(id);
	}
}
