package com.levi.item.controller;

import javax.annotation.Resource;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.levi.item.service.TbItemCatService;

@Controller
public class TbItemCatController {
	
	@Resource
	private TbItemCatService tbItemCatServiceImpl;
	
	@RequestMapping("rest/itemcat/all")
	@ResponseBody
	public String showMenu(String callback){
		String json = JSON.toJSONString(tbItemCatServiceImpl.showCatMenu());
		String result = callback +"(" +json +");";
		return result;
	}
}
